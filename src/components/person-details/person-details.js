import React, { Component } from 'react'
import './person-details.css';
import SwapiService from '../../services/swapi-service';

export default class PersonDetails extends Component {
    swapiService = new SwapiService();
    state = {
        person: null
    };
    componentDidMount() {
        this.updatePerson();
    }
    componentDidUpdate(prevProps) {
        if (this.props.personId !== prevProps.personId) {
            this.updatePerson();
        }
    }
    updatePerson() {
        const { personId } = this.props;
        if(!personId) {
            return;
        }
        this.swapiService.getPerson(personId).then((person) => {
            this.setState({person});
        })
    }
    render() {
        if(!this.state.person) {
            return <span>Select a person from a list</span>;
        }
        const { id, name, gender, birthYear, eyeColor } = this.state.person;
        return (
            <div className="col-md-5 offset-1 person-details py-2">
                <div className="d-flex">
                    <div className="person-img mr-3">
                        <img src={`https://starwars-visualguide.com/assets/img/characters/${id}.jpg`} alt="Person image"/>
                    </div>
                    <div className="person-items">
                        <h4>{name} {this.props.personId}</h4>
                        <ul className="list-unstyled mb-0 mt-2">
                            <li className="person-item">Gender: {gender}</li>
                            <li className="person-item">Birth year: {birthYear}</li>
                            <li className="person-item">Eye color: {eyeColor}</li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}
