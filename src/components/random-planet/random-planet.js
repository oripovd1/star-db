import React, { Component } from 'react';
import './random-planet.css';
import SwapiService from '../../services/swapi-service';
import Spinner from '../spinner/spinner';
import ErrorIndicator from '../error-indicator/error-indicator';

export default class RandomPlanet extends Component {
	swapiService = new SwapiService();
	state = {
		planet: {},
		loading: true
	};
	componentDidMount() {
		this.updatePlanet();
		setInterval(this.updatePlanet, 5000);
	}
	onPlanetLoaded = (planet) => {
		this.setState({ 
			planet,
			loading: false,
			error: false
		});
	}
	onError = (err) => {
		this.setState({
			error: true,
			loading: false
		})
	}
	updatePlanet = () => {
		const id = Math.floor(Math.random()*25) + 3;
		this.swapiService.getPlanet(id).then(this.onPlanetLoaded).catch(this.onError);
	};
	render() {
		const { planet, loading, error } = this.state;
		const hasData = !(loading || error);
		const errorMessage = error ? <ErrorIndicator /> : null;
		const spinner = loading ? <Spinner /> : null;
		const content = hasData ? <PlanetView planet={planet} /> : null;
		return (
			<div>
				<div className="container">
					<div className="row random-planet py-3">
						{ errorMessage }
						{ spinner }
						{ content }
					</div>
				</div>
			</div>
		);
	}
}
const PlanetView = ({ planet }) => {
	const { id, name, population, rotationPeriod, diameter } = planet;
	return(
		<React.Fragment>
			<div className="col-md-3 img-content">
				<img src={`https://starwars-visualguide.com/assets/img/planets/${id}.jpg`} alt="Random planet" className="img-fluid" />
			</div>
			<div className="col-md-3">
				<h1>{name}</h1>
				<ul className="list-unstyled mt-3 ml-3 mb-0">
					<li className="planet-item">Population: <span>{population}</span></li>
					<li className="planet-item">Rotation period: <span>{rotationPeriod}</span></li>
					<li className="planet-item">Diameter: <span>{diameter}</span></li>
				</ul>
			</div>
		</React.Fragment>
	)
}
