import React, { Component } from 'react';
import Header from '../header';
import RandomPlanet from '../random-planet'
import ItemList from '../item-list/item-list';
import PersonDetails from '../person-details/person-details';

export default class App extends Component {
	state = {
		selectedPerson: 15
	}
	onPersonSelected = (id) => {
		this.setState({
			selectedPerson: id
		})
	}
	render() {
		return (
			<div>
				<Header />
				<RandomPlanet />
				<div className="container mt-5">
					<div className="row">
						<ItemList onItemSelected={this.onPersonSelected} />
						<PersonDetails personId={this.state.selectedPerson} />
					</div>
				</div>
			</div>
		);
	}
}
