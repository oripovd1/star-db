import React, {Component} from 'react';
import './header.css';

export default class Header extends Component {
	render() {
		return (
			<div>
				<div className="container">
					<nav className="navbar navbar-expand-lg navbar-dark">
						<a href="localhost:3000" className="navbar-brand">Star DB</a>
						<ul className="navbar-nav ml-5">
							<li className="nav-item">
								<a href="localhost:3000" className="nav-link">People</a>
							</li>
							<li className="nav-item">
								<a href="localhost:3000" className="nav-link">Planets</a>
							</li>
							<li className="nav-item">
								<a href="localhost:3000" className="nav-link">Starships</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		);
	}
}
