import React, { Component } from 'react'

export default class ErrorIndicator extends Component {
    render() {
        return (
            <div className="mx-auto text-warning">
                <p className="h3 text-center">BOOM!</p>
                <p className="m-0">Somethimg has gone terribly wrong</p>
                <span>(but we already sent droids to fix it)</span>
            </div>
        )
    }
}
